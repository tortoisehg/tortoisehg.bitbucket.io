
var info = {
    x64: {keywords: ['win64', 'wow64'],
          desc: '6.9 for Windows 64-bit',
          url: 'https://www.mercurial-scm.org/release/tortoisehg/windows/tortoisehg-6.9-x64.msi'},
    linux2: {keywords: ['linux', 'sunos', 'hp-ux'],
             desc: '6.9 for Linux',
             url: 'https://tortoisehg.bitbucket.io/download/linux.html'},
    darwin: {keywords: ['mac'],
             desc: '6.9 for Mac OS X',
             url: 'https://www.mercurial-scm.org/release/tortoisehg/macos/TortoiseHg-6.9-mac-x64-qt5.dmg'}}

var pf = window.navigator.platform.toLowerCase();
var ua = window.navigator.userAgent.toLowerCase();
for (var name in info) {
    var data = info[name];
    var match = false;
    for (var j in data.keywords) {
        var kw = data.keywords[j];
        if (pf.indexOf(kw) != -1 || ua.indexOf(kw) != -1) {
            match = true;
            break;
        }
    }
    if (match) {
        $('.dlButton > span').text(data.desc);
        $('.dlButton').attr('href', data.url);
        if (name == 'x64') {
            $('#altLink').text('32-Bit')
                         .attr('href', 'https://www.mercurial-scm.org/release/tortoisehg/windows/tortoisehg-6.9-x86.msi');
        } else if (name == 'linux2' || name == 'darwin') {
            $('#altLink').hide();
        }
        break;
    }
}
